/**
*	Lex.java : Contains driver for PA1 that will read Strings from a file into an array and place the indices of those Strings 
*			   into a List in alphabetic order. Will then use the integers held in the List object for outputting Strings from 
*			   input array to a file.
*	Name: Kaile Baran
*	Student ID: 1573206
*	Assignment: Programming Assignment 1
*	Course: CMPS 101
*
*/
import java.io.*;
import java.util.*;

public class Lex 
{
	public static void main(String[] args) throws IOException
	{
		String[] inputArray;
		List list = new List();
		//Checking for number of arguments passed in via command line.
		if(args.length > 2)
		{
			System.err.println("Too many strings given as input.");
			System.exit(1);
		}
		else if(args.length < 2)
		{
			System.err.println("Too few strings given as input.");
			System.exit(2);
		}
		//try catch block for reading from given input file
		try
		{
			int n = 0;
			Scanner fileInput = new Scanner(new File(args[0]));
			//counting the number of lines
			while(fileInput.hasNextLine())
			{
				n++;
				fileInput.nextLine();
			}
			fileInput.close();
			//creating array of size n to hold Strings from input file
			inputArray = new String[n];
			
			fileInput = new Scanner(new File(args[0]));
			int count = 0;
			//re-opening Scanner to read in Strings from input file
			while(fileInput.hasNextLine())
			{
				inputArray[count] = fileInput.nextLine();
				count++;
			}
			list.append(0);
			boolean added = false;
			//Loop for placing indices of Strings in inputArray into List object in the appropriate order.
			for(int i = 1; i < inputArray.length; i++)
			{
				added = false;
				list.moveFront();
				if(list.length() ==1 && inputArray[list.front()].compareTo(inputArray[i]) < 0)
				{
					list.append(i);
				}
				else if(list.length() ==1 && inputArray[list.front()].compareTo(inputArray[i]) > 0)
				{
					list.prepend(i);
				}
				else
				{
					list.moveFront();
					if(inputArray[i].compareTo(inputArray[list.front()]) < 0)
					{
						list.prepend(i);
						added = true;
					}							
					while(list.get() != -1 && inputArray[i].compareTo(inputArray[list.get()]) > 0)
					{
						list.moveNext();
					}
					if(list.get() == -1)
					{
						list.append(i);
					}
					else if(list.get() != -1 && !added)
					{
						list.insertBefore(i);
					}						
				}
			}
			//Sending Strings to output file in order as contained in List object.
			PrintWriter pw = new PrintWriter(new FileWriter(args[1]));
			System.out.println(list.length());
			list.moveFront();
			for(int i = 0; i < list.length(); i++)
			{
				pw.println(inputArray[list.get()]);
				list.moveNext();
			}
			pw.close();
			fileInput.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}		
	}
}

